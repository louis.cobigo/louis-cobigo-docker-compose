# Docker Compose
## Symfony
### Requirements
- MariaDB 10: an available MariaDB server running on port `3306` and host `db` with the username as `symfony`, the password as `symfony` and a database named `symfony`.

### How to run
### How to run
Build the image with :
```sh
docker compose build
```

Launch container with :
```sh
docker compose up -d
```

Install project dependencies :
```sh
docker compose exec app-symfony install
```

Application should be available at [http://localhost:8080](http://localhost:8080) &#x1F44C;
