# Docker Compose
## NextJS
### Requirements
- Node >= 14

### How to run
First thing first : start docker desktop or docker hub.

Go on your shell and paste :
```sh
docker-compose build
```

Then past :
```sh
docker-compose up -d
```


Application should be available at [http://localhost:3000](http://localhost:3000) &#x1F44C;

Modifying the `pages/index.js` file should automatically update and reload the application.