# Docker Compose
## NodeJS + MongoDB
### Requirements
- Node >= 14
- MongoDB: an available MongoDB server running on port `27017` and host `mongo` with the root username as `root` and the root password as `verysecret`.

### How to run
Build the image with :
```sh
docker compose build
```

Launch container with :
```sh
docker compose up -d
```

Application should be available at [http://localhost:3000](http://localhost:3000) &#x1F44C;

Modifying the `views/home/mustache` file should automatically update and reload the application.